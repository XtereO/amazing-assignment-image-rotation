#ifndef MODULE_IMAGE_H
#define MODULE_IMAGE_H
#include "../../models/image/image.h"
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height);
void image_free(struct image img);
uint32_t image_size_row(struct image const *img);

#endif
