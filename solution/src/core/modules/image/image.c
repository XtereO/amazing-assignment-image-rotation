#include "image.h"

struct image image_create(uint64_t width, uint64_t height)
{
    struct image img = {width, height, NULL};
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void image_free(struct image img)
{
    free(img.data);
}

uint32_t image_size_row(struct image const *img)
{
    return sizeof(struct pixel) * img->width;
}
