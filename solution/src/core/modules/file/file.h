#ifndef MODULE_FILE_H
#define MODULE_FILE_H
#include <stdio.h>

FILE *open_file(const char *filename, const char *mode);
void close_file(FILE *file);

#endif
