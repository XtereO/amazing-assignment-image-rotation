#include "file.h"

FILE *open_file(const char *filename, const char *mode)
{
    FILE *file = fopen(filename, mode);
    return file;
}

void close_file(FILE *file)
{
    fclose(file);
}
