#include "bmp.h"

static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;

// count paddings
static size_t bmp_padding(uint64_t width)
{
    return 4 - (width * 3) % 4;
}

// fill image by paddings
static void fill_padding(struct image *img, FILE *out)
{
    uint8_t new_padding = bmp_padding(img->width);
    uint8_t paddings[4];
    for (size_t i = 0; i < new_padding; i++)
    {
        paddings[i] = 0;
    }
    fwrite(paddings, new_padding, 1, out);
}

// get size image of bmp by multiple all pixels on
static uint32_t image_bmp_size(struct image *img)
{
    return (sizeof(struct pixel) * img->width * img->height + bmp_padding(img->width)) * img->height;
}

static struct bmp_header create_bmp_header(struct image *img)
{
    struct bmp_header header;
    header.bfType = bfType;
    header.bfileSize = sizeof(struct bmp_header) + image_bmp_size(img);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = biSize;
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biPlanes = 1;
    header.biBitCount = biBitCount;
    header.biCompression = 0;
    header.biSizeImage = image_bmp_size(img);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    // read bmp_header and validate
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24 || !in)
    {
        return READ_INVALID_BITS;
    }
    if (header.bfType != bfType)
    {
        return READ_INVALID_SIGNATURE;
    }

    // fill pixels
    *img = image_create(header.biWidth, header.biHeight);
    for (int i = 0; i < img->height; i++)
    {
        if (fread(img->data + i * img->width, image_size_row(img), 1, in) != 1)
        {
            return READ_ROW_FAILED;
        }
        if (fseek(in, bmp_padding(img->width), SEEK_CUR))
        {
            return READ_PADDING_FAILED;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header header = create_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1)
    {
        return WRITE_ERROR;
    }

    // remove paddings
    fseek(out, header.bOffBits, SEEK_SET);

    // fill pixels
    if (img->data != NULL)
    {
        for (size_t i = 0; i < img->height; i++)
        {
            fwrite(img->data + i * img->width, image_size_row(img), 1, out);
            fill_padding(img, out);
        }
    }
    return WRITE_OK;
}
