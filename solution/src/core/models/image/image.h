#ifndef MODELE_IMAGE_H
#define MODELE_IMAGE_H
#include "../pixel/pixel.h"

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

#endif
