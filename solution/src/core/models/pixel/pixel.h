#ifndef MODELE_PIXEL_H
#define MODELE_PIXEL_H
#include <stdint.h>

struct pixel
{
    uint8_t b, g, r;
};

#endif
