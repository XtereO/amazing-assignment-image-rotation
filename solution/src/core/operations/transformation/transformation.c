#include "transformation.h"

struct image rotate(const struct image img)
{
    struct image reversed_img = image_create(img.height, img.width);
    for (size_t row = 0; row < img.height; row++)
    {
        for (size_t column = 0; column < img.width; column++)
        {
            reversed_img.data[(column * reversed_img.width) + (reversed_img.width - row - 1)] = img.data[row * img.width + column];
        }
    }
    return reversed_img;
}
