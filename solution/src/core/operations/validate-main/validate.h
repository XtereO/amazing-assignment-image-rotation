#ifndef OPERATION_VALIDATE_MAIN_H
#define OPERATION_VALIDATE_MAIN_H
#include "../../modules/file/file.h"
#include <stddef.h>

int validate_files(FILE *file_in, FILE *file_out);

#endif
