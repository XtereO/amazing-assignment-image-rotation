#include "validate.h"

int validate_files(FILE *file_in, FILE *file_out)
{
    if (file_in == NULL)
    {
        if (file_out == NULL)
        {
            printf("Files doesn't exist \n");
        }
        else
        {
            close_file(file_out);
            printf("Input file doesn't exist \n");
        }
        return 1;
    }
    else
    {
        if (file_out == NULL)
        {
            close_file(file_in);
            printf("Output file doesn't exist \n");
            return 1;
        }
    }
    return 0;
}
