#include "core/modules/bmp/bmp.h"
#include "core/operations/transformation/transformation.h"
#include "core/operations/validate-main/validate.h"

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        printf("Images hasn't provided, but required");
        return 1;
    }

    FILE *file_in = open_file(argv[1], "rb");
    FILE *file_out = open_file(argv[2], "wb");
    if (validate_files(file_in, file_out))
    {
        return 1;
    }

    struct image img = {0, 0, NULL};
    enum read_status read_result = from_bmp(file_in, &img);
    if (read_result != READ_OK)
    {
        printf("Read failed");
        return (int)(read_result);
    }

    struct image rotated_img = rotate(img);
    enum write_status write_result = to_bmp(file_out, &rotated_img);
    if (write_result != WRITE_OK)
    {
        printf("Write failed");
        return (int)(write_result);
    }

    image_free(img);
    image_free(rotated_img);

    close_file(file_in);
    close_file(file_out);

    return 0;
}
